#! /usr/bin/bash
# This is to set up a basic html doc with broiler plate tags already done.

read -p "Enter new page name (without extension): " name
page=".html"
touch $name$page
echo "<!DOCTYPE html>" > $name$page
echo "<html>" >> $name$page
echo "<head>" >> $name$page
echo "<title></title>" >> $name$page
echo '<meta name="" content="">' >> $name$page
echo '<meta name="author" content="Scott R. Hasserd">' >> $name$page
echo '<link rel="stylesheet" type="text/css" href="style.css">' >> $name$page
echo "</head>" >> $name$page
echo "<body>" >> $name$page
echo "</body>" >> $name$page
echo "</html>" >> $name$page


